package com.epam.model.impl;

import com.epam.model.Pizza;
import com.epam.model.PizzaComponents;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Setter
public class VeggiePizza implements Pizza {
    private PizzaComponents components;

    @Override
    public void prepare() {
        log.info("Preparing veggie pizza");
    }

    @Override
    public void bake() {
        log.info("Baking veggie pizza");
    }

    @Override
    public void cut() {
        log.info("Cutting veggie pizza");
    }

    @Override
    public void box() {
        log.info("Boxing veggie pizza");
    }

    @Override
    public PizzaComponents getComponents() {
        return components;
    }
}
