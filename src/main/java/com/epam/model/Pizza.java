package com.epam.model;

public interface Pizza extends WithPizzaComponents {
    void prepare();

    void bake();

    void cut();

    void box();
}
