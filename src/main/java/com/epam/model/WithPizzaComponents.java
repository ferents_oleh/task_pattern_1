package com.epam.model;

public interface WithPizzaComponents {
    PizzaComponents getComponents();
}
