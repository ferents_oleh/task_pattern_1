package com.epam.view;

import com.epam.enums.PizzaType;
import com.epam.factory.bakery.impl.BakeryDnipro;
import com.epam.factory.bakery.impl.BakeryKyiv;
import com.epam.factory.bakery.impl.BakeryLviv;
import com.epam.model.Pizza;
import lombok.extern.log4j.Log4j2;

import java.util.*;

@Log4j2
public class Menu {
    private static Scanner scanner = new Scanner(System.in);

    private Pizza pizza;

    public Menu() {
        while (true) {
            int city = chooseCity();
            int pizzaType = choosePizzaType();
            int pizzaCount = choosePizzaCount();
            makePizza(city, pizzaType, pizzaCount);
            printOrder(pizza, pizzaCount);
        }
    }

    private int chooseCity() {
        log.info("Оберіть ваше місто: \n " +
                "1 - Київ \n " +
                "2 - Львів \n " +
                "3 - Дніпро");
        return scanner.nextInt();
    }

    private int choosePizzaType() {
        log.info("Оберіть піцу: \n " +
                "1 - Clam \n " +
                "2 - Cheese \n " +
                "3 - Veggie");
        return scanner.nextInt();
    }

    private int choosePizzaCount() {
        log.info("Оберіть кількість: ");
        return scanner.nextInt();
    }

    private void makePizza(int city, int pt, int pizzaCount) {
        PizzaType pizzaType = null;

        switch (pt) {
            case 1:
                pizzaType = PizzaType.ClamPizza;
                break;
            case 2:
                pizzaType = PizzaType.CheesePizza;
                break;
            case 3:
                pizzaType = PizzaType.VeggiePizza;
                break;
        }

        for (int i = 0; i < pizzaCount; i++) {
            if (city == 1) {
                pizza = new BakeryKyiv().collect(pizzaType);
            }
            if (city == 2) {
                pizza = new BakeryLviv().collect(pizzaType);
            }
            if (city == 3) {
                pizza = new BakeryDnipro().collect(pizzaType);
            }
        }
    }

    private void printOrder(Pizza pizza, int count) {
        log.info("Order");
        double payment = count * pizza.getComponents().getPrice();
        log.info("Count: " + count);
        log.info("Payment: " + (int) payment);
    }
}

