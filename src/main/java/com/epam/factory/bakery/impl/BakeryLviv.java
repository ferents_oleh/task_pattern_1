package com.epam.factory.bakery.impl;

import com.epam.enums.PizzaType;
import com.epam.factory.bakery.Bakery;
import com.epam.model.Pizza;
import com.epam.model.PizzaComponents;
import com.epam.model.impl.CheesePizza;
import com.epam.model.impl.ClamPizza;
import com.epam.model.impl.VeggiePizza;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class BakeryLviv extends Bakery {
    private PizzaComponents components;

    @Override
    protected Pizza bakePizza(PizzaType pizzaType) {
        if (pizzaType == null) {
            return null;
        }
        if (pizzaType == PizzaType.ClamPizza) {
            components = new PizzaComponents("thick", "marinara", "mushrooms", 610);
            ClamPizza pizza = new ClamPizza();
            pizza.setComponents(components);
            log.info(pizza.getComponents());
            return pizza;
        } else if (pizzaType == PizzaType.CheesePizza) {
            components = new PizzaComponents("thick", "garlic", "cheese", 328);
            CheesePizza pizza = new CheesePizza();
            pizza.setComponents(components);
            log.info(pizza.getComponents());
            return pizza;
        } else if (pizzaType == PizzaType.VeggiePizza) {
            components = new PizzaComponents("thin", "plum tomato", "vegetables", 375);
            VeggiePizza pizza = new VeggiePizza();
            pizza.setComponents(components);
            log.info(pizza.getComponents());
            return pizza;
        }
        return null;
    }
}
