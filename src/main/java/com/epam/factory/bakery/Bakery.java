package com.epam.factory.bakery;

import com.epam.enums.PizzaType;
import com.epam.model.Pizza;

public abstract class Bakery {
    protected abstract Pizza bakePizza(PizzaType pizzaType);

    public Pizza collect(PizzaType pizzaType){
        Pizza pizza = bakePizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
